; Drush Make File
core = 7.x
api = 2
projects[drupal][version] = "7.x"

; Modules
projects[acl][version] = "1.0"
projects[adaptive_image][version] = "1.4"
projects[admin_menu][version] = "3.0-rc3"
projects[advanced_help][version] = "1.0"
projects[autoassignrole][version] = "1.0-beta1"
projects[blockify][version] = "1.2"
projects[ctools][version] = "1.3"
projects[cck][version] = "2.x-dev"
projects[charts_graphs][version] = "1.x-dev"
projects[charts_graphs_flot][version] = "1.x-dev"
projects[ckeditor][version] = "1.13"
projects[colorbox][version] = "1.3"
projects[content_access][version] = "1.2-beta1"
projects[content_taxonomy][version] = "1.0-beta1"
projects[context][version] = "3.0-beta3"
projects[facetapi][version] = "1.0"
projects[date][version] = "2.5"
projects[delta][version] = "3.0-beta9"
projects[devel][version] = "1.2"


projects[devtrac7_admin_feature][download][type] = ""
projects[devtrac7_admin_feature][download][url] = ""
projects[devtrac7_admin_feature][type] = "module"
projects[devtrac7_admin_feature][version] = "1.0-beta"

projects[devtrac7_field_permissions_feature][download][type] = ""
projects[devtrac7_field_permissions_feature][download][url] = ""
projects[devtrac7_field_permissions_feature][type] = "module"
projects[devtrac7_field_permissions_feature][version] = "1.0-beta1"

projects[devtrac7_maps][download][type] = ""
projects[devtrac7_maps][download][url] = ""
projects[devtrac7_maps][type] = "module"
projects[devtrac7_maps][version] = "1.0-beta1"

projects[devtrac7_menu][download][type] = ""
projects[devtrac7_menu][download][url] = ""
projects[devtrac7_menu][type] = "module"
projects[devtrac7_menu][version] = "1.0-beta1"

projects[devtrac7_node_types][download][type] = ""
projects[devtrac7_node_types][download][url] = ""
projects[devtrac7_node_types][type] = "module"
projects[devtrac7_node_types][version] = "1.0-beta1"

projects[devtrac7_questionnaire_feature][download][type] = ""
projects[devtrac7_questionnaire_feature][download][url] = ""
projects[devtrac7_questionnaire_feature][type] = "module"
projects[devtrac7_questionnaire_feature][version] = "1.0-beta1"

projects[devtrac7_services_api][download][type] = ""
projects[devtrac7_services_api][download][url] = ""
projects[devtrac7_services_api][type] = "module"
projects[devtrac7_services_api][version] = "1.0-beta"

projects[devtrac7_site_configuration_feature][download][type] = ""
projects[devtrac7_site_configuration_feature][download][url] = ""
projects[devtrac7_site_configuration_feature][type] = "module"
projects[devtrac7_site_configuration_feature][version] = "1.0-beta"

projects[devtrac7_solr_search_act_item_feature][download][type] = ""
projects[devtrac7_solr_search_act_item_feature][download][url] = ""
projects[devtrac7_solr_search_act_item_feature][type] = "module"
projects[devtrac7_solr_search_act_item_feature][version] = "1.0-beta1"

projects[devtrac7_solr_search_activity_feature][download][type] = ""
projects[devtrac7_solr_search_activity_feature][download][url] = ""
projects[devtrac7_solr_search_activity_feature][type] = "module"
projects[devtrac7_solr_search_activity_feature][version] = "1.0-beta"

projects[devtrac7_solr_search_answers_feature][download][type] = ""
projects[devtrac7_solr_search_answers_feature][download][url] = ""
projects[devtrac7_solr_search_answers_feature][type] = "module"
projects[devtrac7_solr_search_answers_feature][version] = "1.0-beta1"

projects[devtrac7_solr_search_district_feature][download][type] = ""
projects[devtrac7_solr_search_district_feature][download][url] = ""
projects[devtrac7_solr_search_district_feature][type] = "module"
projects[devtrac7_solr_search_district_feature][version] = "1.0"

projects[devtrac7_solr_search_feature][download][type] = ""
projects[devtrac7_solr_search_feature][download][url] = ""
projects[devtrac7_solr_search_feature][type] = "module"
projects[devtrac7_solr_search_feature][version] = "1.0-beta"

projects[devtrac7_solr_search_fieldtrip_feature][download][type] = ""
projects[devtrac7_solr_search_fieldtrip_feature][download][url] = ""
projects[devtrac7_solr_search_fieldtrip_feature][type] = "module"
projects[devtrac7_solr_search_fieldtrip_feature][version] = "1.0-beta1"

projects[devtrac7_solr_search_org_feature][download][type] = ""
projects[devtrac7_solr_search_org_feature][download][url] = ""
projects[devtrac7_solr_search_org_feature][type] = "module"
projects[devtrac7_solr_search_org_feature][version] = "1.0-beta"

projects[devtrac7_solr_search_questions_feature][download][type] = ""
projects[devtrac7_solr_search_questions_feature][download][url] = ""
projects[devtrac7_solr_search_questions_feature][type] = "module"
projects[devtrac7_solr_search_questions_feature][version] = "1.0-beta1"

projects[devtrac7_solr_user_feature][download][type] = ""
projects[devtrac7_solr_user_feature][download][url] = ""
projects[devtrac7_solr_user_feature][type] = "module"
projects[devtrac7_solr_user_feature][version] = "1.0-beta"

projects[devtrac7_taxonomy_feature][download][type] = ""
projects[devtrac7_taxonomy_feature][download][url] = ""
projects[devtrac7_taxonomy_feature][type] = "module"
projects[devtrac7_taxonomy_feature][version] = "1.0-beta"

projects[devtrac7_theme_feature][download][type] = ""
projects[devtrac7_theme_feature][download][url] = ""
projects[devtrac7_theme_feature][type] = "module"
projects[devtrac7_theme_feature][version] = "1.0"

projects[devtrac7_users2][download][type] = ""
projects[devtrac7_users2][download][url] = ""
projects[devtrac7_users2][type] = "module"
projects[devtrac7_users2][version] = "1.0-beta1"

projects[devtrac7_views_feature][download][type] = ""
projects[devtrac7_views_feature][download][url] = ""
projects[devtrac7_views_feature][type] = "module"
projects[devtrac7_views_feature][version] = "1.0"

projects[diff][version] = "2.0"

projects[district_wikipedia_import][download][type] = ""
projects[district_wikipedia_import][download][url] = ""
projects[district_wikipedia_import][type] = "module"
projects[district_wikipedia_import][version] = "1.0-beta1"

projects[draggableviews][version] = "2.0-beta1"
projects[ds][version] = "1.8"
projects[email][version] = "1.2"
projects[entity][version] = "1.0-rc3"
projects[entityreference][version] = "1.0-rc3"
projects[entityreference_prepopulate][version] = "1.0"
projects[facetapi_bonus][version] = "1.1"
projects[facetapi_graphs][version] = "1.x-dev"
projects[facetapi_tagcloud][version] = "1.0-beta1"
projects[features][version] = "1.0-rc2"
projects[feeds][version] = "2.0-alpha4"
projects[feeds_tamper][version] = "1.0-beta3"
projects[field_collection][version] = "1.0-beta3"
projects[field_collection_table][version] = "1.0-beta1"
projects[field_group][version] = "1.1"
projects[field_permissions][version] = "1.0-beta2"
projects[file_entity][version] = "2.0-unstable7"
projects[flot][version] = "1.0+4-dev"
projects[geofield][version] = "2.0"
projects[geonames][version] = "1.1"
projects[geophp][version] = "1.7"
projects[hidden_comment][version] = "1.7+3-dev"

projects[iati_feature][download][type] = ""
projects[iati_feature][download][url] = ""
projects[iati_feature][type] = "module"
projects[iati_feature][version] = "1.0-beta"

projects[job_scheduler][version] = "2.0-alpha2"
projects[kml][version] = "1.0-alpha1+1-dev"
projects[libraries][version] = "2.1"
projects[link][version] = "1.0"
projects[maxlength][version] = "3.0-beta1"
projects[media][version] = "2.0-unstable7"
projects[memcache][version] = "1.0"
projects[menu_token][version] = "1.0-beta1"
projects[migrate][version] = "2.4"
projects[nice_menus][version] = "2.1"
projects[omega_tools][version] = "3.0-rc3"
projects[openlayers][version] = "2.0-beta7+30-dev"
projects[openlayers_cck_migrate][version] = "1.0-alpha1"
projects[openlayers_geosearch][version] = "1.0-beta1"
projects[openlayers_plus][version] = "3.0-beta1"
projects[piwik][version] = "2.4"
projects[prepopulate][version] = "2.x-dev"
projects[print][version] = "1.1"
projects[profile_switcher][version] = "1.0-beta1"
projects[proj4js][version] = "1.2"
projects[publishcontent][version] = "1.0"
projects[quicktabs][version] = "3.4"
projects[realname][version] = "1.1"
projects[relation][version] = "1.0-rc4"
projects[relation_add][version] = "1.0-beta1"
projects[services][version] = "3.1"
projects[robotstxt][version] = "1.0"
projects[search_api][version] = "1.2"
projects[search_api_autocomplete][version] = "1.0"
projects[search_api_saved_searches][version] = "1.1"
projects[search_api_solr][version] = "1.3"
projects[search_api_solr_overrides][version] = "1.0-rc1"
projects[services_views][version] = "1.x-dev"
projects[strongarm][version] = "2.0-rc1"
projects[taxonomy_access][version] = "1.0-rc1"
projects[tipsy][version] = "1.0-rc1"
projects[token][version] = "1.1"

projects[usaid_import][download][type] = ""
projects[usaid_import][download][url] = ""
projects[usaid_import][type] = "module"
projects[usaid_import][version] = "1.0-beta"

projects[views][version] = "3.7"
projects[views_hacks][version] = "1.0-alpha1"
projects[views_charts][version] = "1.x-dev"
projects[views_data_export][version] = "3.0-beta6"
projects[views_field_view][version] = "1.0-rc3"
projects[views_infinite_scroll][version] = "1.1"
projects[views_modes][version] = "1.x-dev"
projects[views_rss][version] = "2.0-rc3"
projects[views_rss_georss][version] = "1.0-rc1"
projects[views_slideshow][version] = "3.0+18-dev"
projects[wms][version] = "2.x-dev"

; Themes
projects[omega][version] = "3.1"

; Modules
projects[OL_lat_long][type] = "module"
projects[OL_lat_long][download][type] = "git"
projects[OL_lat_long][download][url] = "http://git.drupal.org/sandbox/GwenMahe/2055375.git"
projects[OL_lat_long][download][revision] = "e02a7b1ec36a1d073c8c68565deba67a6428f0d5"

projects[comment_block_d7][download][type] = ""
projects[comment_block_d7][download][url] = ""
projects[comment_block_d7][type] = "module"

projects[custom_breadcrumbs][download][type] = ""
projects[custom_breadcrumbs][download][url] = ""
projects[custom_breadcrumbs][type] = "module"

projects[custom_breadcrumbs_identifiers][download][type] = ""
projects[custom_breadcrumbs_identifiers][download][url] = ""
projects[custom_breadcrumbs_identifiers][type] = "module"

projects[custom_breadcrumbs_panels][download][type] = ""
projects[custom_breadcrumbs_panels][download][url] = ""
projects[custom_breadcrumbs_panels][type] = "module"

projects[custom_breadcrumbs_paths][download][type] = ""
projects[custom_breadcrumbs_paths][download][url] = ""
projects[custom_breadcrumbs_paths][type] = "module"

projects[custom_breadcrumbs_taxonomy][download][type] = ""
projects[custom_breadcrumbs_taxonomy][download][url] = ""
projects[custom_breadcrumbs_taxonomy][type] = "module"

projects[custom_breadcrumbs_views][download][type] = ""
projects[custom_breadcrumbs_views][download][url] = ""
projects[custom_breadcrumbs_views][type] = "module"

projects[custom_breadcrumbsapi][download][type] = ""
projects[custom_breadcrumbsapi][download][url] = ""
projects[custom_breadcrumbsapi][type] = "module"

projects[date_duration_formatter][download][type] = ""
projects[date_duration_formatter][download][url] = ""
projects[date_duration_formatter][type] = "module"

projects[devtrac7][download][type] = ""
projects[devtrac7][download][url] = ""
projects[devtrac7][type] = "module"

projects[devtrac7_access][download][type] = ""
projects[devtrac7_access][download][url] = ""
projects[devtrac7_access][type] = "module"

projects[devtrac7_fieldtrip_progress][download][type] = ""
projects[devtrac7_fieldtrip_progress][download][url] = ""
projects[devtrac7_fieldtrip_progress][type] = "module"

projects[devtrac7_geosearch][download][type] = ""
projects[devtrac7_geosearch][download][url] = ""
projects[devtrac7_geosearch][type] = "module"

projects[devtrac7_migrate][download][type] = ""
projects[devtrac7_migrate][download][url] = ""
projects[devtrac7_migrate][type] = "module"

projects[devtrac7_mymenu][download][type] = ""
projects[devtrac7_mymenu][download][url] = ""
projects[devtrac7_mymenu][type] = "module"

projects[devtrac7_progress_field][download][type] = ""
projects[devtrac7_progress_field][download][url] = ""
projects[devtrac7_progress_field][type] = "module"

projects[devtrac7_purpose_migration][download][type] = ""
projects[devtrac7_purpose_migration][download][url] = ""
projects[devtrac7_purpose_migration][type] = "module"

projects[devtrac7_realms][download][type] = ""
projects[devtrac7_realms][download][url] = ""
projects[devtrac7_realms][type] = "module"

projects[devtrac7_solr_search_location_feature][download][type] = ""
projects[devtrac7_solr_search_location_feature][download][url] = ""
projects[devtrac7_solr_search_location_feature][type] = "module"

projects[devtrac7_views][download][type] = ""
projects[devtrac7_views][download][url] = ""
projects[devtrac7_views][type] = "module"

projects[devtrac7_wms][download][type] = ""
projects[devtrac7_wms][download][url] = ""
projects[devtrac7_wms][type] = "module"

projects[devtrac7test][download][type] = ""
projects[devtrac7test][download][url] = ""
projects[devtrac7test][type] = "module"

projects[devtrac_saved_search][download][type] = ""
projects[devtrac_saved_search][download][url] = ""
projects[devtrac_saved_search][type] = "module"

projects[devtrac_taxonomy][download][type] = ""
projects[devtrac_taxonomy][download][url] = ""
projects[devtrac_taxonomy][type] = "module"

projects[facet_api_context][download][type] = ""
projects[facet_api_context][download][url] = ""
projects[facet_api_context][type] = "module"

projects[facetapi_textfield][download][type] = ""
projects[facetapi_textfield][download][url] = ""
projects[facetapi_textfield][type] = "module"

projects[geocoder][download][type] = ""
projects[geocoder][download][url] = ""
projects[geocoder][type] = "module"

projects[geocoder_geonames][type] = "module"
projects[geocoder_geonames][download][type] = "git"
projects[geocoder_geonames][download][url] = "http://git.drupal.org/sandbox/dmulindwa/1996068.git"
projects[geocoder_geonames][download][branch] = "7.x-1.x"
projects[geocoder_geonames][download][revision] = "57417b6bab16585f8d06a06e460e5096d3ba25b0"

projects[helpinject][download][type] = ""
projects[helpinject][download][url] = ""
projects[helpinject][type] = "module"

projects[helpinject_help][download][type] = ""
projects[helpinject_help][download][url] = ""
projects[helpinject_help][type] = "module"

projects[openlayers_geosearch_wfs][download][type] = ""
projects[openlayers_geosearch_wfs][download][url] = ""
projects[openlayers_geosearch_wfs][type] = "module"

projects[profile_migration][download][type] = ""
projects[profile_migration][download][url] = ""
projects[profile_migration][type] = "module"

projects[proxy][download][type] = ""
projects[proxy][download][url] = ""
projects[proxy][type] = "module"

projects[purl][download][type] = ""
projects[purl][download][url] = ""
projects[purl][type] = "module"

projects[purl_devtrac][download][type] = ""
projects[purl_devtrac][download][url] = ""
projects[purl_devtrac][type] = "module"

projects[purl_search_api][download][type] = ""
projects[purl_search_api][download][url] = ""
projects[purl_search_api][type] = "module"

projects[tests][download][type] = ""
projects[tests][download][url] = ""
projects[tests][type] = "module"

projects[questionnaire][type] = "module"
projects[questionnaire][download][type] = "git"
projects[questionnaire][download][url] = "http://git.drupal.org/sandbox/batje/1509732.git"
projects[questionnaire][download][revision] = "c6515a2236d3bae5e602a19a0ef8facfc7fc5987"

projects[references_migrate][download][type] = ""
projects[references_migrate][download][url] = ""
projects[references_migrate][type] = "module"

projects[search_api_date][download][type] = ""
projects[search_api_date][download][url] = ""
projects[search_api_date][type] = "module"

projects[search_api_swatches][download][type] = ""
projects[search_api_swatches][download][url] = ""
projects[search_api_swatches][type] = "module"

projects[views_two_column_table][download][type] = ""
projects[views_two_column_table][download][url] = ""
projects[views_two_column_table][type] = "module"

projects[virtual_field][download][type] = ""
projects[virtual_field][download][url] = ""
projects[virtual_field][type] = "module"

; Themes
projects[devtrac7_theme][download][type] = ""
projects[devtrac7_theme][download][url] = ""
projects[devtrac7_theme][type] = "theme"

; Libraries
libraries[FirePHPCore][download][type] = ""
libraries[FirePHPCore][download][url] = ""
libraries[FirePHPCore][directory_name] = "FirePHPCore"
libraries[FirePHPCore][type] = "library"

libraries[flot][download][type] = ""
libraries[flot][download][url] = ""
libraries[flot][directory_name] = "flot"
libraries[flot][type] = "library"

libraries[colorbox][download][type] = ""
libraries[colorbox][download][url] = ""
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"

libraries[SolrPhpClient][download][type] = ""
libraries[SolrPhpClient][download][url] = ""
libraries[SolrPhpClient][directory_name] = "SolrPhpClient"
libraries[SolrPhpClient][type] = "library"

libraries[json2][download][type] = ""
libraries[json2][download][url] = ""
libraries[json2][directory_name] = "json2"
libraries[json2][type] = "library"

libraries[jquery.cycle][download][type] = ""
libraries[jquery.cycle][download][url] = ""
libraries[jquery.cycle][directory_name] = "jquery.cycle"
libraries[jquery.cycle][type] = "library"

libraries[openlayers][download][type] = ""
libraries[openlayers][download][url] = ""
libraries[openlayers][directory_name] = "openlayers"
libraries[openlayers][type] = "library"

libraries[ckeditor][download][type] = ""
libraries[ckeditor][download][url] = ""
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"

libraries[autopager][download][type] = ""
libraries[autopager][download][url] = ""
libraries[autopager][directory_name] = "autopager"
libraries[autopager][type] = "library"

libraries[tcpdf][download][type] = ""
libraries[tcpdf][download][url] = ""
libraries[tcpdf][directory_name] = "tcpdf"
libraries[tcpdf][type] = "library"

